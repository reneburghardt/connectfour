
public class ConnectFour {
	
	int[][] board = new int[7][6];
	Player rp1 = new RandomPlayer(1);
	Player rp2 = new RandomPlayer(2);
	int lastTurn;
	
	public ConnectFour() {	/*
		board[3][3] = 2;	//Initialisiere Brett zum Testen
		board[3][1] = 2;	
		board[2][2] = 2;
		board[0][0] = 2;
		board[1][0] = 2;
		board[2][0] = 2; */
	}
	
	public int checkWon() {
		int a;
		int won = 0;
		for(int j=0; j < 6; j++) { //check horizontally
			for(int i=0; i < 4; i++) {
				if(this.board[i][j] != 0) {
					a = this.board[i][j];
					won = a;
					for(int k = i+1; k < i+4; k++) {
						if(this.board[k][j] != a) {
							won = 0;
							break;
						}
					}
					if(won != 0) return won;
				}
			}
		}
		
		for(int j=0; j < 7; j++) { //check vertically
			for(int i=0; i < 3; i++) {
				if(this.board[j][i] != 0) {
					a = this.board[j][i];
					won = a;
					for(int k = i+1; k < i+4; k++) {
						if(this.board[j][k] != a) {
							won = 0;
							break;
						}
					}
					if(won != 0) return won;
				}
			}
		}
		
		for(int j=0; j < 3; j++) { //diagonally right
			for(int i=0; i < 4; i++) {
				if(this.board[i][j] != 0) {
					a = this.board[i][j];
					won = a;
					for(int k = i+1, l = j+1; k < i+4; k++, l++) {
						if(this.board[k][l] != a) {
							won = 0;
							break;
						}
					}
					if(won != 0) return won;
				}
			}
		}
		
		for(int j=0; j < 3; j++) { //diagonally left
			for(int i=6; i > 2; i--) {
				if(this.board[i][j] != 0) {
					a = this.board[i][j];
					won = a;
					for(int k = i-1, l = j+1; k > i-4; k--, l++) {
						if(this.board[k][l] != a) {
							won = 0;
							break;
						}
					}
					if(won != 0) return won;
				}				
			}
		}
		
		for(int i=0; i < 7; i++) { //test for remis
			if(this.board[i][5] == 0) return 0;
		}
		
		return -1;
	}
	
	public boolean checkTurn(int x) {
		if(x < 7 && this.board[x][5] == 0) return true;
		return false;
	}
	
	public void setTurn(int player, int x) {
		for(int i=0; i < 6; i++) {
			if(this.board[x][i] == 0) {
				this.board[x][i] = player;
				break;
			}
		}
	}
	
	public void getTurn(int player) {
		int turn;
		if(player == 2) {
			turn = rp2.getTurn(board);
		}
		else{
			turn = rp1.getTurn(board);
		}
		if(this.checkTurn(turn)) {
			this.setTurn(player, turn);
			this.lastTurn = turn;
		}
	}
	
	public String toString() {
		String game = "";
		for(int j = 5; j >= 0; j--) {
			for(int i = 0; i < 7; i++) {
				game += this.board[i][j]+" ";
			}
			game += "\n";
		}
		return game;
	}
	
	public void run() {
		int player = 2;
		System.out.println("Start:");
		System.out.println(this);
		int turn = 1;
		int check = this.checkWon();
		while(check == 0) {
			if(player == 1) player = 2;
			else player = 1;
			System.out.println("-----------------------");
			System.out.println((turn++)+". Turn: Player "+player);
			this.getTurn(player);
			System.out.println(this);
			check = this.checkWon();
		}
		if(check == -1) System.out.println("Remis");
		else System.out.println("Player "+player+" won!!!");
	}

	public static void main(String[] args) {
		ConnectFour cf = new ConnectFour();
		for(int a = 0; a < 10; a++) {cf.run();cf = new ConnectFour();}
	}

}
