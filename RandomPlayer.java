import java.util.Random;


public class RandomPlayer extends Player{
	public RandomPlayer(int player) {
		super(player);
	}
	public int getTurn(int[][] board) {
		Random rand = new Random();
	    int randomNum = (rand.nextInt((7 - 1) + 1) + 1)-1;
	    while(board[randomNum][5] != 0) {
	    	randomNum = (rand.nextInt((7 - 1) + 1) + 1)-1;
	    }
	    return randomNum;
	}
}
