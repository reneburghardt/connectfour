
public abstract class Player {
	int player;
	public Player(int player) {
		this.player = player;
	}
	public abstract int getTurn(int[][] board);
}
